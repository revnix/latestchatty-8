﻿using Latest_Chatty_8.Common;
using Latest_Chatty_8.DataModel;
using Latest_Chatty_8.Networking;
using Latest_Chatty_8.Settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Latest_Chatty_8.Views
{
    public class ReplyNavParameter
    {
        public Comment Comment { get; private set; }
        public Comment RootComment { get; private set; }

        public ReplyNavParameter(Comment c, Comment rootComment)
        {
            this.Comment = c;
            this.RootComment = rootComment;
        }
    }
	/// <summary>
	/// A basic page that provides characteristics common to most applications.
	/// </summary>
	public sealed partial class ReplyToCommentView : Latest_Chatty_8.Common.LayoutAwarePage
	{
		#region Private Variables
        private ReplyNavParameter navParam;
		private bool ctrlPressed = false;
		
		#endregion

		#region Constructor
		public ReplyToCommentView()
		{
			this.InitializeComponent(); 
			Window.Current.SizeChanged += WindowSizeChanged;
		} 
		#endregion

		#region Events
		private void WindowSizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
		{
			this.LayoutUI();
		}

		async private void SendButtonClicked(object sender, RoutedEventArgs e)
		{
			await this.SendReply();
		}

		async private void AttachClicked(object sender, RoutedEventArgs e)
		{
			if (Windows.UI.ViewManagement.ApplicationView.Value == Windows.UI.ViewManagement.ApplicationViewState.Snapped)
			{
				var dialog = new MessageDialog("Can't attach photos in snapped view.");
				return;
			}

			this.progress.IsIndeterminate = true;
			this.progress.Visibility = Windows.UI.Xaml.Visibility.Visible;
			this.postButton.IsEnabled = false;
			this.attachButton.IsEnabled = false;

			try
			{
				var photoUrl = await ChattyPics.UploadPhoto();
				await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Low, () =>
				{
					this.replyText.Text += photoUrl;
				});
			}
			finally
			{
				this.progress.IsIndeterminate = false;
				this.progress.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
				this.postButton.IsEnabled = true;
				this.attachButton.IsEnabled = true;
			}
		}
		#endregion

		#region Overrides
		async protected override Task<bool> CorePageKeyActivated(CoreDispatcher sender, AcceleratorKeyEventArgs args)
		{
			await base.CorePageKeyActivated(sender, args);
			//If it's not a key down event, we don't care about it.
			if (args.EventType != CoreAcceleratorKeyEventType.SystemKeyDown &&
				 args.EventType != CoreAcceleratorKeyEventType.KeyDown)
			{
				return true;
			}

			var ctrlDown = (Window.Current.CoreWindow.GetKeyState(VirtualKey.Control) & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;
			switch (args.VirtualKey)
			{
				case Windows.System.VirtualKey.Enter:
					if (ctrlPressed)
					{
						await this.SendReply();
					}
					break;
				default:
					break;
			}
			return true;
		} 
		#endregion

		#region Load and Save State
		protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
		{
			this.LayoutUI();
            this.navParam = navigationParameter as ReplyNavParameter;
          
			if (this.navParam != null)
			{
				this.DefaultViewModel["ReplyToComment"] = this.navParam.Comment;
			}
			else
			{
				//Making a root post.  We don't need this.
				this.commentBrowser.Visibility = Visibility.Collapsed;
				this.replyGrid.RowDefinitions.RemoveAt(0);
			}
		}

		protected override void SaveState(Dictionary<String, Object> pageState)
		{
			Window.Current.SizeChanged -= WindowSizeChanged;
		} 
		#endregion

		#region Private Helpers
		async private Task SendReply()
		{
			var button = postButton;

			if (this.replyText.Text.Length <= 5)
			{
				var dlg = new Windows.UI.Popups.MessageDialog("Post something longer.");
				await dlg.ShowAsync();
				return;
			}

			try
			{
				this.bottomBar.Focus(Windows.UI.Xaml.FocusState.Programmatic);
				button.IsEnabled = false;

				this.progress.IsIndeterminate = true;
				this.progress.Visibility = Windows.UI.Xaml.Visibility.Visible;

				var content = this.replyText.Text;

				var encodedBody = Uri.EscapeUriString(content);
				content = "body=" + encodedBody;
				//If we're not replying to a comment, we're root chatty posting.
				if (this.navParam != null)
				{
					content += "&parent_id=" + this.navParam.Comment.Id;
                    if (LatestChattySettings.Instance.AutoPinOnReply)
                    {
                        LatestChattySettings.Instance.AddPinnedComment(this.navParam.RootComment);
                    }

				}

				await POSTHelper.Send(Locations.PostUrl, content, true);
                
				CoreServices.Instance.PostedAComment = true;
				this.Frame.GoBack();
				return;
			}
			catch
			{
			}
			finally
			{
				this.progress.IsIndeterminate = false;
				this.progress.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
				button.IsEnabled = true;
			}

			var d = new MessageDialog("There was an error posting.  Please try again.", "Uh oh!");
			await d.ShowAsync();
		}

		private void LayoutUI()
		{
			if (Windows.UI.ViewManagement.ApplicationView.Value == Windows.UI.ViewManagement.ApplicationViewState.Snapped)
			{
				WebBrowserBinding.SetFontSize(this.web, 10);
				if (Window.Current.Bounds.Left == 0) //Snapped Left side.
				{
					this.postButton.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
					return;
				}
			}

			if (Windows.UI.ViewManagement.ApplicationView.Value != Windows.UI.ViewManagement.ApplicationViewState.Snapped)
			{
				WebBrowserBinding.SetFontSize(this.web, 14);
			}

			this.postButton.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
		} 
		#endregion
	}
}
